# BackpackExchangeBot
此程式為幣本位網格，金額設定夠多就會越跑幣越多
```
以下說明如果出現這種框框，皆代表為「命令提示字元」或「終端機」

```


## 環境架設(Windows)
- [ ] [Node.js](https://nodejs.org/en/download)
```
node -v
```
如果沒有會出現版本號的話去下載，選擇 Windowns Installer 


- [ ] [Git](https://git-scm.com/download/win)
```
git -v
```
如果沒有會出現版本號的話去下載，從 Standalone Installer 中選擇自己電腦適合的


## 環境架設(Mac)
- [ ] [Node.js](https://nodejs.org/en/download) 
```
node -v
```
如果沒有會出現版本號的話去下載，選擇 Mac Installer 


- [ ] Homebrew install
```
brew -v
```
如果沒有會出現版本號的話接著輸入
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```


- [ ] Git install
```
git -v
```
如果沒有會出現版本號的話接著輸入
```
brew install git
```


## 主程式下載
環境架設完成後，透過命令下載主程式。

請先將「命令提示字元」或「終端機」中的目錄移動到你想要的存放的位置資料夾

下列命令中「金鑰」請私下跟我要，然後替換掉
```
cd 你想要的位置
git clone https://ChihChienKung:金鑰@gitlab.com/ChihChienKung/backpackexchangebot.git
```

## API key
從背包的[API設定頁](https://backpack.exchange/settings/api-keys)申請一組KEY

1.點「New API Key」

2.填入名稱

3.點「Generate」

4.將取得的「Key」「Secret」填入我們下載好的主程式中 config.js 內(可用記事本開啟)


## config.js 參數說明
- [ ] 除了金額之為所有設定只能支持到小數點後兩位

API_KEY: 從背包申請的 Key     

API_secret: 從背包申請的 secret

orderUSDC: 每一注下單金額(會換算成SOL)

safeyOrderCount: 上下行開單數量

orderGap: 網格間距

limitDown: 網格開單下限

limitUp: 網格開單上限

defaultPrice: 一般為 undefined ，如果上一次斷線想從同一個位置接回就輸入上一次最後的定位價格


## 啟動程式
記得要先移動到主程式的目錄下
```
node index.js
```

## 更新程式
記得要先移動到主程式的目錄下
```
git pull
```
如果提示金鑰已經過期，先更新金鑰後再執行一次上面命令
```
git remote set-url origin https://ChihChienKung:金鑰@gitlab.com/ChihChienKung/backpackexchangebot.git
```

## 錯誤排除
以下操作可能會要求你輸入電腦密碼
- [ ] Error: Cannot find module 'got'
```
sudo npm install got
```
